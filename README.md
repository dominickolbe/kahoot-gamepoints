# kahoot-gamepoints

> kahoot coding challenge

## Development

install
```
$ yarn
```

install watchman if needed
```
$ brew install watchman
```

start dev server
```
$ yarn start

  Compiled successfully!

  You can now view kahoot-gamepoints in the browser.

  Local:            http://localhost:3000/
  On Your Network:  http://X.X.X.X:3000/
```

run tests
```
$ yarn test
```


### Info
created and tested with:
* macOS Sierra ```v10.13.5```
* Node.js ```v9.8.0```
* yarn ```1.3.2```
* npm ```5.6.0```
