import { createStore, applyMiddleware } from 'redux'
import { middleware as reduxPackMiddleware } from 'redux-pack'
import { createLogger } from 'redux-logger'
import reducer from './reducer'

const middleware = [reduxPackMiddleware]
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

const configureStore = () => createStore(reducer, applyMiddleware(...middleware))

export default configureStore
