import * as types from '../actiontypes'

// add item to game
export const addItem = item => {

  // the reason why i'm using promises is only
  // to show how i would solve async problems
  const promise = new Promise(async resolve => {
    resolve({
      ...item,
      time: + new Date(),
    })
  })

  return {
    type: types.ADD_ITEM,
    promise
  }
}

// reset game / new game
export const resetGame = () => ({
  type: types.RESET_GAME,
})
