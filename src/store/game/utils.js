import { bonus } from './setup'

// build stats object for overview
export const buildGameStats = (items = []) => {

  const gameStats = {}

  items.forEach(item => {

    if (!(item.mode in gameStats)) {

      gameStats[item.mode] = {
        mode: item.mode,
        qty: 0,
        score: 0,
      }
    }

    gameStats[item.mode].qty++
    gameStats[item.mode].score += item.points

  })

  return gameStats
}

// get total amount of points without bonus
export const getTotalPoints = items => {
  let total = 0

  items.forEach(item => {
    total += item.points
  })

  return total
}

// get total amount of bonus
export const getTotalBonus = gameStats => {
  let total = 0

  Object.keys(gameStats).forEach(key => {
    bonus.forEach(bonusitem => {
      if (gameStats[key].mode === bonusitem.mode) {
        if (gameStats[key].qty >= bonusitem.qty) {
          total += (bonusitem.bonus * parseInt(gameStats[key].qty / bonusitem.qty, 0))
        }
      }
    })
  })

  return total
}
