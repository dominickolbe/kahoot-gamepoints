import { handle } from 'redux-pack'
import { initialState } from './selectors'
import * as types from '../actiontypes'

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    // add new item to game
    case types.ADD_ITEM:
      return handle(state, action, {
        success: prevState => ({
          ...prevState,
          items: [
            ...prevState.items,
            payload,
          ],
        })
      })

    case types.RESET_GAME:
      // return initialState == reset game
      return initialState
    default:
      return state
  }
}
