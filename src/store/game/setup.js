// available game items and points
// is used to render game buttons
export const gameItems = [{
  mode: 'A',
  points: 50,
}, {
  mode: 'B',
  points: 30,
}, {
  mode: 'C',
  points: 20,
}, {
  mode: 'D',
  points: 15,
}]

// bonus setup
// here you can setup which bonus you get
// after a certain amount of items
export const bonus = [{
  mode: 'A',
  qty: 3,
  bonus: 200,
}, {
  mode: 'B',
  qty: 2,
  bonus: 90,
}]
