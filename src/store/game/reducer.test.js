import { LIFECYCLE, KEY } from 'redux-pack'
import { initialState } from './selectors'
import reducer from './reducer'
import * as types from '../actiontypes'
import { gameItems } from './setup'

describe('Reducer: Settings', () => {
  it('should return the default state if the action type does not match a case', () => {
    const action = {}
    expect(reducer(initialState, action)).toEqual(initialState)
  })

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('action ADD_ITEM is working as expected', () => {
    const action = {
      type: types.ADD_ITEM,
      payload: gameItems[0],
      meta: {
        [KEY.LIFECYCLE]: LIFECYCLE.SUCCESS,
      },
    }
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      items: [
        ...initialState.items,
        gameItems[0],
      ]
    })
  })

  it('action RESET_GAME is working as expected', () => {
    const action = {
      type: types.RESET_GAME,
      payload: { test: 1234 }
    }
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
    })
  })
})
