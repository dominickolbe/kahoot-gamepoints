import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getGame } from './store/selectors'
import { Button, Tag, notification } from 'antd'
import { addItem, resetGame } from './store/game/actions'
import { gameItems } from './store/game/setup'
import { buildGameStats, getTotalPoints, getTotalBonus } from './store/game/utils'

class App extends Component {
  render() {

    const { game } = this.props
    const gameStats = buildGameStats(game.items)
    const totalPoints = getTotalPoints(game.items)
    const totalBonus = getTotalBonus(gameStats)

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h1>Kahoot! Points</h1>

            {/* render all available game buttons */}
            { gameItems.map(item => (
              <Button
                key={item.mode}
                onClick={() => {
                  this.props.dispatch(addItem(item))
                }}>
                  {item.mode}
              </Button>
            ))}

          </div>
          <div className="col-md-6">
            <h1>PLAYER ITEMS</h1>

            <div className="row">
              <div className="col-4">
                <b>ITEM</b>
              </div>
              <div className="col-4">
                <b>QTY</b>
              </div>
              <div className="col-4">
                <b>SCORE</b>
              </div>
            </div>

            {/* render game stats */}
            { Object.keys(gameStats).map(key => (
              <div key={key} className="row">
                <div className="col-4">
                  <Tag>{ gameStats[key].mode }</Tag>
                </div>
                <div className="col-4">
                  { gameStats[key].qty }
                </div>
                <div className="col-4">
                  { gameStats[key].score }
                </div>
              </div>
            ))}

            <hr />

            <div className="row">
              <div className="col-8">
                <h3>Bonus:</h3>
              </div>
              <div className="col-4">
                <Tag color="blue">{ totalBonus }</Tag>
              </div>
            </div>

            <div className="row">
              <div className="col-8">
                <h3>Total:</h3>
              </div>
              <div className="col-4">
                <Tag color="blue">{ totalPoints+totalBonus }</Tag>
              </div>
            </div>

            <hr />

            <Button
              onClick={() => {
                this.props.dispatch(resetGame())

                notification['success']({
                  message: 'NEW GAME',
                  description: 'Your game has been restarted.',
                })
              }}>
                NEW GAME
            </Button>

          </div>
        </div>
      </div>
    )
  }
}

App.propTypes = {
  game: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  game: getGame(state),
})

export default connect(mapStateToProps)(App)
